package com.example.artisla.simplecalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.TextView;
import android.view.View.OnClickListener;

import java.text.DecimalFormat;

public class MainActivity extends Activity implements OnClickListener {
    TextView solution;
    TextView input;

    String currentNumber = "",operand1,operand2,operation,answer;
    Double num1,num2;
    Boolean isFirstOperationDone = false;
    DecimalFormat decimalFormat = new DecimalFormat("#.####");

    Button add,subtract,multiply,divide,decimal,backSpace,one,two,three,four,five,six,seven,eight,nine,zero,clear,equal;





    public void loadButton() {
        add = (Button) findViewById(R.id.btnAdd);
        subtract = (Button)findViewById(R.id.btnSubtract);
        multiply = (Button)findViewById(R.id.btnMultiply);
        divide = (Button)findViewById(R.id.btnDivide);
        decimal = (Button)findViewById(R.id.btnDecimal);
        backSpace = (Button)findViewById(R.id.btnBack);
        one = (Button)findViewById(R.id.btnOne);
      input = (TextView)findViewById(R.id.input);
        solution = (TextView)findViewById(R.id.Solution);
        two = (Button)findViewById(R.id.btnTwo);
        three = (Button)findViewById(R.id.btnThree);
        four = (Button)findViewById(R.id.btnFour);
        five = (Button)findViewById(R.id.btnFive);
        six = (Button)findViewById(R.id.btnSix);
        seven = (Button)findViewById(R.id.btnSeven);
        eight = (Button)findViewById(R.id.btnEight);
        nine = (Button)findViewById(R.id.btnNine);
        zero =(Button)findViewById(R.id.btnZero);
        clear = (Button)findViewById(R.id.btnClear);
        equal = (Button)findViewById(R.id.btnEquals);

        equal.setOnClickListener(this);
        add.setOnClickListener(this);
        subtract.setOnClickListener(this);
        multiply.setOnClickListener(this);
        divide.setOnClickListener(this);
        decimal.setOnClickListener(this);
        backSpace.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);
        clear.setOnClickListener(this);

    }
    public void refreshInput(){
        if(currentNumber.length()<17 ) {
        input.setText(currentNumber);}
        else{
            Toast.makeText(this,"Too long Number",Toast.LENGTH_SHORT).show();
        }
        }

    public void removeLastCharacter(){
        if (currentNumber.length()>0) {
            currentNumber = currentNumber.substring(0, currentNumber.length()-1);
        }
    }

    public void setDecimal(){
        if(!currentNumber.contains(".")){
            currentNumber += ".";
        }

    }


    public void calculate(){
    try{
        if(operand2 != "" && operand1 != "" ){
            num1 = Double.parseDouble(operand1);
            num2 = Double.parseDouble(operand2);
            if(operation == "+")
            {
                solution.setText(String.valueOf(num1 + num2));
                answer = String.valueOf(num1 + num2);
                isFirstOperationDone = true;
                currentNumber="";
                refreshInput();

            }
            else if (operation == "-")
            {
                solution.setText(String.valueOf(num1-num2));
                answer = String.valueOf(num1 - num2);
                isFirstOperationDone = true;
                currentNumber="";
                refreshInput();

            }
            else if(operation == "*")
            {
                solution.setText(String.valueOf(num1*num2));
                answer = String.valueOf(num1 * num2);
                isFirstOperationDone = true;
                currentNumber="";
                refreshInput();

            }
            else {
                solution.setText(decimalFormat.format(num1 / num2));
                answer = decimalFormat.format(num1 / num2);
                isFirstOperationDone = true;
                currentNumber="";
                refreshInput();

            }

        }}
    catch (Exception error){
        Toast.makeText(this,error.toString(),Toast.LENGTH_LONG).show();
        Toast.makeText(this,operand1 +"   " + operand2+ "   "+operation,Toast.LENGTH_LONG).show();
    }
    }





    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnOne:
                currentNumber += "1";
                refreshInput();
                break;
            case R.id.btnTwo:
                currentNumber += "2";
                refreshInput();
                break;
            case R.id.btnZero:
                currentNumber += "0";
                refreshInput();
                break;
            case R.id.btnThree:
                currentNumber +="3";
                refreshInput();
                break;
            case R.id.btnFour:
                currentNumber += "4";
                refreshInput();
                break;
            case R.id.btnFive:
                currentNumber += "5";
                refreshInput();
                break;
            case R.id.btnSix:
                currentNumber += "6";
                refreshInput();
                break;
            case R.id.btnClear:
                currentNumber = "";
                operand1 = "";
                operand2 = "";
                isFirstOperationDone = false;
                solution.setText("");
                refreshInput();
                break;
            case R.id.btnBack:
                removeLastCharacter();
                refreshInput();
                break;
            case R.id.btnSeven:
                currentNumber += "7";
                refreshInput();
                break;
            case R.id.btnEight:
                currentNumber += "8";
                refreshInput();
                break;
            case R.id.btnNine:
                currentNumber += "9";
                refreshInput();
                break;
            case R.id.btnDecimal:
                setDecimal();
                refreshInput();
                break;
            case R.id.btnAdd:
                operand1 = currentNumber;
                currentNumber = "";
                operation = "+";
                break;
            case  R.id.btnMultiply:
                operand1 = currentNumber;
                currentNumber = "";
                operation = "*";
                break;
            case  R.id.btnDivide:
                operand1 = currentNumber;
                currentNumber = "";
                operation = "/";
                break;
            case R.id.btnSubtract:
                operand1 = currentNumber;
                currentNumber = "";
                operation = "-";
                break;
            case R.id.btnEquals:
                operand2 = currentNumber;
                if(isFirstOperationDone) {
                secondOperation();
                }

                calculate();
                break;


        }

    }
    public void secondOperation(){
        if(answer.length() != 0)
        operand1 = answer;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadButton();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
